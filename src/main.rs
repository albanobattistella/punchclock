// SPDX-License-Identifier: GPL-3.0-or-later

#[macro_use]
extern crate gtk_rust_app;
#[macro_use]
extern crate log;

use gtk::prelude::*;

use store::{gstore::dispatch, store};

use crate::{
    pages::{AboutPage, Settings, TaskList},
    widgets::Pill,
};

mod domain;
mod pages;
mod store;
mod widgets;

fn main() {
    env_logger::init();

    store::init_store(
        crate::store::State::default(),
        store::reduce,
        vec![
            crate::store::data::SelectionMiddleware::new(),
            crate::store::data::PersistenceMiddleware::new(),
        ],
    );
    dispatch!(crate::store::_INIT);

    gtk_rust_app::app(
        include_bytes!("../Cargo.toml"),
        include_bytes!("../App.toml"),
        include_bytes!("../target/gra-gen/compiled.gresource"),
        None,
    )
    .store(store::store())
    .styles(include_str!("styles.css"))
    .build(
        |application, project_descriptor, settings| {
            widgets::init();
            let pill = Pill::new();
            pill.add_page(TaskList::new());
            pill.add_page(Settings::new());
            pill.add_page(AboutPage::new(&project_descriptor.package.name));

            let window = gtk_rust_app::window(
                application,
                project_descriptor.package.name.clone(),
                settings,
                pill.upcast_ref(),
            );
            // window.move
            window.connect_show(glib::clone!(@weak pill => move |_w| {
                pill.expand();
            }));
            window.show();
        },
        |app, _project_descriptor, _settings| {
            if let Some(action) = app.lookup_action("quit") {
                let simple_action: gdk4::gio::SimpleAction = action.downcast().unwrap();
                simple_action.connect_activate(glib::clone!(@weak app => move |_, _| {
                    app.windows().first().unwrap().close();
                    app.quit();
                }));
            }
        },
    );
}

// SPDX-License-Identifier: GPL-3.0-or-later

use crate::store::{dispatch, store};
use adw::subclass::prelude::{ActionRowImpl, PreferencesRowImpl};
use gdk4::subclass::prelude::IsSubclassable;
use gtk::{prelude::*, subclass::list_box_row::ListBoxRowImpl};
use libadwaita as adw;

use super::RemoveRow;
use super::RemoveRowImpl;

#[widget(@store extends RemoveRow)]
#[template(file = "task_row.ui")]
pub struct TaskRow {
    #[property_string]
    pub task: std::cell::Cell<String>,
    #[signal_handler(self activated)]
    on_activated: (),
    #[signal_handler(self remove)]
    on_remove: (),
    #[signal_handler(self pre_remove)]
    on_before_remove: (),
}

unsafe impl IsSubclassable<imp::TaskRow> for RemoveRow {}
impl ActionRowImpl for imp::TaskRow {}
impl PreferencesRowImpl for imp::TaskRow {}
impl ListBoxRowImpl for imp::TaskRow {}

impl TaskRow {
    pub fn new(task: &str) -> Self {
        glib::Object::new(&[("task", &task)]).expect("Failed to create Search")
    }

    pub fn constructed(&self) {}

    pub fn set_subtitle(&self, subtitle: &str) {
        self.set_property("subtitle", subtitle)
    }

    pub fn set_label(&self, label: &str) {
        self.set_property("label", label);
    }

    pub fn task(&self) -> String {
        self.property::<String>("task")
    }

    pub fn needs_submit(&self) -> bool {
        self.property("needs_submit")
    }

    fn on_activated(&self, row: RemoveRow) {
        if !row.needs_submit() {
            dispatch!(crate::store::_SELECT_TASK, row.title());
        }
    }
    fn on_remove(&self, row: Self) {
        let title: String = row.property("title");
        dispatch!(crate::store::_DELETE, title);
    }
    fn on_before_remove(&self, _: Self) {
        self.remove_css_class("active_task")
    }
}

impl Default for TaskRow {
    fn default() -> Self {
        Self::new("")
    }
}

// SPDX-License-Identifier: GPL-3.0-or-later

use crate::store::{store, State};
use gdk4::subclass::prelude::ObjectSubclassIsExt;
use gtk::{prelude::*, TemplateChild};
use gtk_rust_app::{gstore::dispatch, widgets::Page};
use libadwaita as adw;

use super::{Clock, MenuHeaderButton, Search};

// This is a 'smart' widget. It has access to the global store and is therefore more entangled with the overall application.
#[widget(@store extends gtk::Box)]
#[template(file = "pill.ui")]
pub struct Pill {
    #[template_child]
    pub clock: TemplateChild<Clock>,
    #[template_child]
    pub search: TemplateChild<Search>,
    #[template_child]
    pub main: TemplateChild<gtk::Widget>,
    #[template_child]
    pub view_stack: TemplateChild<adw::ViewStack>,
    #[template_child]
    pub expand_button: TemplateChild<gtk::Button>,
    #[template_child]
    pub play_button: TemplateChild<gtk::Button>,
    #[template_child]
    pub collapse_button: TemplateChild<gtk::Button>,
    #[template_child]
    pub menu: TemplateChild<MenuHeaderButton>,
    #[template_child]
    pub collapse_expand_box: TemplateChild<gtk::Box>,

    #[signal_handler(expand_button clicked)]
    pub on_expand_clicked: (),

    #[signal_handler(play_button clicked)]
    pub on_play_pause_clicked: (),

    #[signal_handler(collapse_button clicked)]
    pub on_collapse_clicked: (),

    #[selector(state.clock.running)]
    select_running: (),

    #[selector(state.history.queue)]
    select_history: (),
}

impl Pill {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create Pill")
    }

    pub fn constructed(&self) {
        self.expand_button().set_visible(false);
    }

    pub fn add_page(&self, page: impl Page + IsA<gtk::Widget>) {
        self.view_stack().add_named(&page, Some(page.name()));
    }

    pub fn expand(&self) {
        let window: adw::ApplicationWindow = self.root().unwrap().dynamic_cast().unwrap();
        let s = self;
        window.connect_maximized_notify(glib::clone!(@weak s => move |window| {
            s.collapse_expand_box().set_visible(!window.is_maximized())
        }));
        self.main().set_visible(true);
    }

    fn on_collapse_clicked(&self, b: gtk::Button) {
        let main = self.main();
        let window: adw::ApplicationWindow = b.root().unwrap().dynamic_cast().unwrap();
        main.set_visible(false);
        window.set_height_request(-1);
        window.set_default_height(54);
        main.set_visible(true);
        dispatch!(crate::store::BACK)
    }

    fn on_expand_clicked(&self, _: gtk::Button) {
        self.do_expand();
    }

    fn do_expand(&self) {
        let window: adw::ApplicationWindow = self.root().unwrap().dynamic_cast().unwrap();
        window.set_height_request(400);
        let (s, r) = glib::MainContext::channel(glib::Priority::default());
        r.attach(
            None,
            glib::clone!(@weak window => @default-return glib::Continue(false), move |_| {
                window.set_height_request(-1);
                glib::Continue(false)
            }),
        );
        std::thread::spawn(move || {
            std::thread::sleep(std::time::Duration::from_millis(10));
            s.send(())
        });
    }

    fn on_play_pause_clicked(&self, _: gtk::Button) {
        dispatch!(crate::store::_TOGGLE);
    }

    fn select_running(&self, state: &State) {
        if state.clock.running {
            self.play_button()
                .set_icon_name("media-playback-stop-symbolic")
        } else {
            self.play_button()
                .set_icon_name("media-playback-start-symbolic")
        }
    }

    fn select_history(&self, state: &State) {
        if let Some(name) = state.history.queue.last() {
            self.view_stack().set_visible_child_name(name);
            if state.history.queue.len() > 1 {
                self.do_expand()
            }
        }
    }
}

impl Default for Pill {
    fn default() -> Self {
        Self::new()
    }
}

// SPDX-License-Identifier: GPL-3.0-or-later

use crate::store::{data::select_overall_time, dispatch, store, State};
use gdk4::subclass::prelude::ObjectSubclassIsExt;
use gtk::{prelude::*, TemplateChild};

#[widget(@store extends gtk::Box)]
#[template(file = "clock.ui")]
pub struct Clock {
    #[template_child]
    pub label: TemplateChild<gtk::Label>,

    #[selector(state.data.timings)]
    select_timings: (),
}

impl Clock {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create Clock")
    }

    pub fn constructed(&self) {
        // glib
        self.handle_time_update()
    }

    fn handle_time_update(&self) {
        let (s, r) = glib::MainContext::channel(glib::PRIORITY_DEFAULT_IDLE);
        let label = self.label();
        r.attach(
            None,
            glib::clone!(@weak label => @default-return glib::Continue(true), move |i| {
                if i == 0 {
                    dispatch!(crate::store::_SAVE);
                }
                dispatch!(crate::store::_TICK);
                glib::Continue(true)
            }),
        );

        std::thread::spawn(move || {
            let mut i: i16 = 0;
            loop {
                s.send(i).expect("Failed to send time tick.");
                std::thread::sleep(std::time::Duration::from_secs(1));
                i = (i + 1).rem_euclid(3);
            }
        });
    }

    fn select_timings(&self, state: &crate::store::State) {
        if let Some(latest) = state.data.timings.last() {
            let name = latest.name();
            let (h, m, s) = select_overall_time(state, name, false);
            self.label()
                .set_text(&format!("{:02}:{:02}:{:02}", h, m, s));
        }
    }
}

impl Default for Clock {
    fn default() -> Self {
        Self::new()
    }
}

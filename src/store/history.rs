// SPDX-License-Identifier: GPL-3.0-or-later

use crate::pages::TASK_LIST_PAGE_NAME;

use super::{Action, State};

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct History {
    pub queue: Vec<String>,
}

impl Default for History {
    fn default() -> Self {
        Self {
            queue: vec![TASK_LIST_PAGE_NAME.into()],
        }
    }
}

pub fn reduce(action: &Action, state: &mut State) {
    match action.name() {
        crate::store::NAVIGATE => {
            let name: String = action.arg().unwrap();
            state.history.queue.push(name);
        }
        crate::store::BACK => {
            if state.history.queue.len() > 1 {
                state.history.queue.pop();
            }
        }
        _ => {}
    }
}

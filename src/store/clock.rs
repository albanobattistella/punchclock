// SPDX-License-Identifier: GPL-3.0-or-later

use crate::domain::Timing;

use super::{Action, State};

#[derive(Debug, Default, Clone, PartialEq, Eq)]
pub struct Clock {
    pub running: bool,
}
pub mod actions {
    pub const _START: &str = ".start";
    pub const _TICK: &str = ".tick";
    pub const _STOP: &str = ".stop";
    pub const _TOGGLE: &str = ".toggle";
}

pub fn reduce(action: &Action, state: &mut State) {
    match action.name() {
        actions::_TICK => {
            if state.clock.running {
                if let Some(timing) = state.data.timings.last_mut() {
                    if timing.start().date() == time::OffsetDateTime::now_utc().date() {
                        timing.tick();
                    } else {
                        let name = timing.name().into();
                        state.data.timings.push(Timing::new(name, 1))
                    }
                }
            }
        }
        actions::_TOGGLE => {
            state.clock.running = !state.clock.running;
        }
        actions::_START => {
            state.clock.running = true;
        }
        actions::_STOP => {
            state.clock.running = false;
        }
        _ => {}
    }
}

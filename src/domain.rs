// SPDX-License-Identifier: GPL-3.0-or-later

use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Timing {
    name: String,
    start: i64,
    time: u64,
}

impl Timing {
    pub fn new(name: String, time: u64) -> Self {
        Self {
            name,
            start: time::OffsetDateTime::now_utc().unix_timestamp(),
            time,
        }
    }
    pub fn start(&self) -> time::OffsetDateTime {
        time::OffsetDateTime::from_unix_timestamp(self.start)
            .unwrap_or(time::OffsetDateTime::UNIX_EPOCH)
    }
    pub fn name(&self) -> &str {
        &self.name
    }
    pub fn time(&self) -> u64 {
        self.time
    }
    pub fn tick(&mut self) {
        self.time += 1;
    }
}

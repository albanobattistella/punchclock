# punchclock

[![pipeline status](https://gitlab.com/floers/punchclock/badges/main/pipeline.svg)](https://gitlab.com/floers/punchclock/-/commits/main)

![](assets/icon.128.png)

A time tracking app for linux.

![](screenshots/small.png)

![](screenshots/large.png)


## Support

Wether you submit issues or contribute code: I appreciate all help.

## Installation: Flatpak

`Punchclock` is a flatpak app and available on [flathub](https://flathub.org/apps/details/codes.loers.Punchclock).

It can be installed and started with:

```shell
# flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
flatpak install codes.loers.Punchclock
flatpak run codes.loers.Punchclock
```
